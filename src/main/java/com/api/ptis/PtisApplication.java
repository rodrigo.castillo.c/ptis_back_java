package com.api.ptis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PtisApplication {

	public static void main(String[] args) {
		SpringApplication.run(PtisApplication.class, args);
	}

}
